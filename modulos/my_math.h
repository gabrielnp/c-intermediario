#ifndef MY_MATH_H_
#define MY_MATH_H_

int fatorial(int n);
int area_quadrado(int lado);
int area_retangulo(int altura, int base);

#endif /* MY_MATH_H_ */
