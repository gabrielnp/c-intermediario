#include <iostream>

using namespace std;

void msg(int n){
	cout << "Number: " << n << endl;
}

void msg(){
	cout << "Hello World!" << endl;
}

int main(){
	msg(10);
	msg();
	return 0;

}
