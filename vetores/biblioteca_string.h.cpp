/*
 * STRING.H
 *
 * strlen => retorna o tamanho da string
 *
 * isalpha => é um boolean. Retorna true para caracter alfabético e false para caracter numérico
 *
 * isdigit => é um boolean. Retorna treu para numérico e false para alfabético
 *
 * isupper => é um boolean. Retorna true para letra maiscula e false para minuscula
 * islower => o contrário
 *
 * isspace => é um boolean. Verifica se é um espaço em branco
 *
 * tolower => transforma a string em caracteres minúsculos
 * toupper => transforma a string em caracteres maiusculos
 *
 * strcmp(nome1,nome2) => compara se as strings são iguais
 *
 *
 *strcat(destino, origem) Ex: strcat(nome, sobrenome);
 *
 * */

