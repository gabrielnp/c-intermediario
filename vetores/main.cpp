#include <iostream>
#include <string.h>

using namespace std;

void inverte(char nomeStr[]){
	// obtendo o tamanho da string
	int tam;
	for(tam = 0; nomeStr[tam]; tam++);

	for (int i = tam-1; i>=0;i--)
		cout << nomeStr[i];
	cout << " está invertido" << endl;
}

int main(){
	// A declaração abaixo é igual a esta:  int vetorInteiros [] = {1,2,3,4}
	int vetorInteiros[4] = {1,2,3,4};
	// ou seja, o tamanho do vetor pode ser omitido se ele for inicializado dessa forma.

	cout << "Tamanho do vetor em bytes" << endl;
	cout << "Inteiro: " << sizeof(vetorInteiros) << endl; // cada elemento de um vetor de inteiros tem 4bytes

	char vetorChar[] = {'1','2','3','4'};

	cout << "Char: " << sizeof(vetorChar) << endl; // cada elemento de um vetor char tem 1 byte


	/* ------------------------------------------------------------------------------- */

	char nome[] = {'g','a','b','r','i','e','l','\0'};
	int i = 0;

	while(nome[i])// essa comparação é a mesma que nome[i] != '\0'
		cout << nome[i++];
	cout << endl;

	char nomeStr[] = "marcos";
	char nome2[] = "gabriela";

	if(strcmp(nome,nome2) == 0)
		cout << nome << " e "<< nome2 << " são strings iguais" << endl;
	else
		cout << nome << " e "<< nome2 << " são strings diferentes" << endl;

	inverte(nomeStr);

	char primeiroNome[100], sobrenome[100];

	cout << "Digite seu nome: ";
	cin >> primeiroNome;
	cout << "Digite seu sobrenome: ";
	cin >> sobrenome;

	strcat(nome,sobrenome);
	cout << "seu nome completo é: " << nome;


	return 0;
}



