#include <iostream>

using namespace std;

bool par(int num){
	if(num % 2 == 0)
		return true;
	return false;
}

void mensagem(); // função que será chamada depois da main em algum lugar deste arquivo

int main(){
	int n;

	cout << "Digite um número: ";
	cin >> n;
	if(par(n)){
		cout << "O número " << n << " é par" << endl;;
	}else
		cout << "O número " << n << " é ímpar" << endl;

	return 0;

	mensagem();
}

void mensagem(){
	cout << "aprendendo C++" << endl;
 }
